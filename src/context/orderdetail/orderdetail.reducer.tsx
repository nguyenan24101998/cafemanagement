import { OrderDetail, OrderDetailWithIdOnly } from "../../type"
import { orderType } from "../type"

const { UPDATE_ORDER, UPDATE_DETAIL, UPDATE_TOTAL, SET_LOADING
    , SET_SAVE_DIALOG, SET_CLEAR_DIALOG, SET_CACHE, SET_CACHE_DETAIL, SET_CACHE_TOTAL } = orderType

export const initialState = {
    total: 0,
    orders: [] as OrderDetail[],
    orderDetail: [] as OrderDetailWithIdOnly[],  // for saving to firebase
    cacheOrder: [],
    cacheOrderDetail: [],
    cacheTotal: [],
    loading: false,
    isShowSaveDialog: false,
    isShowClearDialog: false,
    addOrUpdateOrder: (newItem) => {},
    clearOrder: () => {},
    increaseAmount: (index, unitPrice) => {},
    decreaseAmount: (index, unitPrice) => {},
    undoHandler: () => {},
    saveOrderHandler: () => {},
    setShowSaveDialog: (isShow) => {},
    setShowClearDialog: (isShow) => {},
}

const ordersReducer = (state = initialState, action) => {
    const { type, payload } = action
    
    switch (type){
        case UPDATE_ORDER:
            return { 
                ...state, 
                orders: payload.orders
            }

        case UPDATE_DETAIL:
            return { 
                ...state, 
                orderDetail: payload.orderDetail
            }

        case SET_CACHE:
            return {
                ...state, 
                cacheOrder: payload.cacheOrder
            }

        case SET_CACHE_DETAIL:
            return {
                ...state, 
                cacheOrderDetail: payload.cacheOrderDetail
            }

        case SET_CACHE_TOTAL:
            return {
                ...state, 
                cacheTotal: payload.cacheTotal
            }

        case UPDATE_TOTAL:
            return {
                ...state,
                total: payload.total
            }

        case SET_LOADING:
            return { 
                ...state, 
                loading: payload.loading 
            }

        case SET_SAVE_DIALOG:
            return { 
                ...state, 
                isShowSaveDialog: payload.isShowSaveDialog
            }

        case SET_CLEAR_DIALOG:
            return { 
                ...state, 
                isShowClearDialog: payload.isShowClearDialog 
            }
        
        default: 
            return state
    }
}

export default ordersReducer