import { ReactElement, createContext, useEffect, useReducer, useState } from "react";
import itemsReducer, { initialState } from "./items.reducer";
import { string } from '../../core/string'
import DB from '../../database/local'
import { Item } from '../../models/item'
import { itemsType } from "../type";

export const ItemContext = createContext(initialState)
const { SET_LOADING, GET_ITEMS, ADD_ITEMS } = itemsType

const ItemProvider = ({children}): ReactElement => {
    ///const [items, setItems] = useState([])
    const [state, dispatch] = useReducer(itemsReducer, initialState)

    const fetchItem = async () => {
        try{
            await DB.open()

            let items: Item[] = await DB.getItems()
            
            if (items.length <= 0){  //Data is null/app's first installation
                console.log('Create default items and save to database')
                
                items = [
                new Item(string.label.darkCafe, '~/dark-coffee.png',
                    14000
                ),

                new Item(string.label.milkCafe, '~/milk-coffee.png',
                    17000
                ),

                new Item(string.label.vietnameseCafe, '~/vietnamese-milk-coffee.png',
                    20000
                ),

                new Item(string.label.creamyCafe, '~/creamy-coffee.png',
                    22000
                ),
                
                new Item(string.label.saltedCafe, '~/salted-coffee.png',
                    20000
                ),]

                await DB.addItems(items)
            }

            dispatch({
                type: GET_ITEMS,
                payload: items || []
            })
            
        } catch (error: any) {
            console.log(error.message)
        }

        setLoading(false)
    }

    const setLoading = (isLoad) => {
        dispatch({
            type: SET_LOADING,
            payload: {
                loading: isLoad
            }
        })
    }
    
    const addItems = (items) => {
        return async dispatch => {
            try {
                await DB.addItems(items)
                
                dispatch({
                    type: ADD_ITEMS,
                    payload: { ...items }
                })
            } catch (error: any) {
                console.log(error.message)
            }
        }
    }

    useEffect(() => {
        fetchItem()
        //DB.deleteAllTables()
    }, [])

    const value = {
        items: state.items,
        loading: state.loading,
        setLoading,
        addItems
    }

    return (
        <ItemContext.Provider value={value}>{children}</ItemContext.Provider>
    )
}

export default ItemProvider