import { DimensionValue, View } from "react-native";
import { theme } from "../core/theme";
import { memo } from "react";

type Props = {
    width: DimensionValue
    styles: any,
}

const VerticalLine = ({width, styles}: Props) => (
    <View style={[{
        backgroundColor: theme.colors.accent,
        width: width,
        height: 1,
      },
      styles
    ]} />
)

VerticalLine.defaultProps = {
    styles: {}
}

export default memo(VerticalLine)