import {useEffect, useState} from 'react';
import {Dimensions} from 'react-native';

export const screenRotaion = {
    PORTRAIT: "PORTRAIT",
    LANDSCAPE: "LANDSCAPE"
}

export const useOrientation = () => {
    const { PORTRAIT, LANDSCAPE } = screenRotaion
    const [orientation, setOrientation] = useState(PORTRAIT);

    useEffect(() => {
      Dimensions.addEventListener('change', ({window:{width,height}})=>{
        if (width < height) {
          setOrientation(PORTRAIT)
        } else {
          setOrientation(LANDSCAPE)
      
        }
      })

    }, []);

    return orientation;
}