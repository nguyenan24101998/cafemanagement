import React, { memo } from "react";
import { Button } from "react-native-paper";
import { theme } from "../core/theme";

type Props = React.ComponentProps<typeof Button>;

const TextButton = ({ children, ...props }: Props) => (
  <Button
    //buttonColor={theme.colors.primary}
    labelStyle={{color: theme.colors.accent}}
    mode="text"
    {...props}>
    {children}
  </Button>
);

export default memo(TextButton);