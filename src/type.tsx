import { Item } from "./models/item";


export type Navigation = {
    navigate: (scene: string, params: any) => void;
}

export type OrderDetail = {
    item: Item,
    amount: number
}

export type OrderDetailWithIdOnly = { 
    itemId: string;
    amount: number
}

export type groupOrderDate = {
    date: string,
    orders: [{
        id: string,
        time: string,
        items: OrderDetailWithIdOnly[],
        total: number
    }]
}