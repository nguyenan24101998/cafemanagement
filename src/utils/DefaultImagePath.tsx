export const defaultItemImageCollection = {
    '~/dark-coffee.png': require('../assets/dark-coffee.png'),
    '~/milk-coffee.png': require('../assets/milk-coffee.png'),
    '~/vietnamese-milk-coffee.png': require('../assets/vietnamese-milk-coffee.png'),
    '~/creamy-coffee.png': require('../assets/creamy-coffee.png'),
    '~/salted-coffee.png': require('../assets/salted-coffee.png'),
}