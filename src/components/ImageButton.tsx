import { ReactNode, memo } from 'react'
import { 
    TouchableOpacity, 
    Image, 
    StyleProp, 
    ViewStyle, 
    ImageStyle,
    TextStyle,
    ImageSourcePropType, 
    
} from "react-native";
import Text from '../components/Text'

type Props = {
    style: StyleProp<ViewStyle>;
    imageStyle: StyleProp<ImageStyle>; 
    textStyle: StyleProp<TextStyle>; 
    onPress(): void;
    source: ImageSourcePropType;
    label: string;
    children: ReactNode
}

const ImageButton = ({style, imageStyle, textStyle, onPress, source, label, children}: Props ) => (
    <TouchableOpacity
        activeOpacity={0.4}
        style={style}
        onPress={onPress}>
        
        <Image 
            style={imageStyle}
            source={source}/>

        {children}
        <Text style={textStyle}>{label}</Text>
    </TouchableOpacity>
)

ImageButton.defaultProps = {
    style: {},
    imageStyle: {},
    textStyle: {},
    label: "",
    onPress: null,
    children: {}
}

export default memo(ImageButton)