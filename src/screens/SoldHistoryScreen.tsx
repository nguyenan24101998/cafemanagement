import React, { ReactNode, memo, useEffect, useState } from 'react';
import { Alert, FlatList, ScrollView, StyleSheet, Text } from 'react-native';
import DB from '../database/local'
import { string } from '../core/string';
import StatusTextWrapper from '../components/StatusTextWrapper';
import { List } from 'react-native-paper';
import FireStore from '../database/firestore';
import { Order } from '../models/order';
import Loading from '../components/Loading';
import { Navigation, groupOrderDate } from '../type';
import { Item } from '../models/item';

const SoldHistoryScreen = ({ route }) =>{
    const [loading, setLoading] = useState(true)
    const [defaultAlertString, setDefaultAlertString] = useState(string.status.empty)
    const [items, setItems] = useState<Item[]>([])
    const [orders, setOrders] = useState<groupOrderDate[]>([])

    const fetchOrdersData = async () => {
        setItems(route.params?.items)

        console.log(route)
        
        // await FireStore.getOrders(
        //     (resultData) => {
        //         setOrders(resultData)
        //         setLoading(false)
        //         setDefaultAlertString("")
        //     },
        //     () => {
        //         setLoading(false)
        //         setDefaultAlertString("Lỗi dữ liệu")
        //     }
        // )
        
    }

    useEffect(() => {
        fetchOrdersData()
    }, [])

    const getItemDetail = (itemId: String) => {
        return items.find(e => e.id == itemId)
    }

    return (
        <StatusTextWrapper
            conditionToShowText={ orders.length <= 0 }
            text={ defaultAlertString }>

            <FlatList
                data={orders}
                keyExtractor={(_, index) => String(index)}
                renderItem={ ({item: headerRow}) =>
                    <List.Accordion
                        title={headerRow.date}
                        left={ props => <List.Icon {...props} icon="calendar"/> }>
                        
                        <FlatList
                            data={headerRow.orders}
                            keyExtractor={(_, index) => String(index)}
                            renderItem={ ({item: orderRow}) =>
                                <>
                                    <List.Item title={`${orderRow.time}-----${getItemDetail(orderRow.items[0].itemId)?.label} x${orderRow.items[0].amount}`} />
                                
                                    <FlatList
                                        data={orderRow.items}
                                        keyExtractor={(_, index) => String(index)}
                                        renderItem={ ({item}) =>
                                        <List.Item title={`--------------${getItemDetail(item.itemId)?.label} x${item.amount}`} />
                                    }/>
                                </>
                            }/>
                        
                    </List.Accordion>
                }/>

            {/* {loading ? <Loading/> : null} */}
            
        </StatusTextWrapper>
    )
};

export default memo(SoldHistoryScreen)