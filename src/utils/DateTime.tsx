export const  getToday = () =>{
    const formatter = new Intl.DateTimeFormat('vi-VN', { day: 'numeric', month: 'long', year: 'numeric'})
    return (formatter.format(new Date()))
}

export const  getTime = () =>{
    const formatter = new Intl.DateTimeFormat('vi-VN', {hour:'numeric', minute: 'numeric'})
    return (formatter.format(new Date()))
}