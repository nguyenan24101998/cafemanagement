import React, { memo, useContext, useEffect, useState } from 'react';
import { theme } from '../../core/theme';
import { FAB } from 'react-native-paper';
import { Order } from '../../models/order';
import StatusTextWrapper from '../../components/StatusTextWrapper';
import { OrderDetail } from "../../type"
import Loading from '../../components/Loading';
import ItemList from './ItemList';
import OrderView from './OrderView';
import OrderProvider from '../../context/orderdetail/orderdetail.context';
import { ItemContext } from '../../context/items/item.context'
import { screenRotaion, useOrientation } from '../../utils/ScreenRotation';

const HomeScreen = ({ navigation }) => {
  const { loading } = useContext(ItemContext)
  const [defaultAlertString, setDefaultAlertString] = useState("")
  const { PORTRAIT } = screenRotaion
  const orientation = useOrientation()

  const [order, setOrder] = useState(new Order())
  const [orderDetails, setOrderDetails] = useState<OrderDetail[]>([])
  const [total, setTotal] = useState(0)
  const [isShowDialog, setShowDialog] = useState(false)

  const _orderSubmitHandler = async () => {

    setShowDialog(false)
    //setLoading(true)
    
    orderDetails.map(detail => {
      order.items.push({itemId: detail.item.id, amount: detail.amount})
    })

    order.total = total
  }

  return (
    <>
      <StatusTextWrapper
        conditionToShowText={defaultAlertString != ""}
        text={defaultAlertString}>
          
          <OrderProvider>
            {
              orientation === PORTRAIT ? 
              <ItemList numColumns={ 2 } />
              :<ItemList numColumns={ 4 } />
            }
            
            <OrderView/>
              
          </OrderProvider>
          
          <FAB style={{ backgroundColor:theme.colors.primary, position: 'absolute', right:20, bottom: 300}} 
            icon='menu'
            color={theme.colors.accent}
            onPress={() => { navigation.navigate('SoldHistoryScreen') }}/>
          
          <Loading isShow={loading}/>
          
      </StatusTextWrapper>
    </>
  );
};

export default memo(HomeScreen);