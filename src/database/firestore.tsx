import firestore from '@react-native-firebase/firestore';
import { Order } from '../models/order';
import { groupOrderDate } from '../type';
import { Item } from '../models/item';


const itemsCollectionName = 'items'
const orderCollectionName = 'orders'

type resultHandler = {
    onSuccess: (resultData) => void;
    onFailed: () => void
}


class FireStore {
    static itemsCollection = firestore().collection(itemsCollectionName)
    static ordersCollection = firestore().collection(orderCollectionName)

    static addItems = async (items: Item[], { onSuccess, onFailed }: resultHandler) => {
        const batch = firestore().batch()
        
        items.forEach(doc => {
            const docRef = this.itemsCollection.doc(doc.id)
            batch.set(docRef, doc)
        })

        return batch.commit()
            .then(_=> { onSuccess(null) })
            .catch(error => {
                onFailed()
                console.log(error)
            })
    }

    static getItems = async ({ onSuccess, onFailed }: resultHandler) => {
        return await this.itemsCollection
            .onSnapshot((querySnapshot => {
                const resultData: any[] = []
                
                querySnapshot.forEach(doc => {
                    resultData.push(doc.data())
                })

                onSuccess(resultData)
            }), error => {
                onFailed()
                console.log(error)
            })
    }

    static addOrUpdateOrder = async (order: Order, { onSuccess, onFailed }: resultHandler) => {    
        return await this.ordersCollection
                .doc(order.id)
                .set({
                    id: order.id,
                    date: order.date,
                    time: order.time,
                    items: order.items,
                    total: order.total,
                    paid: order.isPaid
                }).then(_ => {
                    onSuccess(null)
                }).catch(error => {
                    onFailed()
                    console.log(error)
                })
                
    }

    static getOrders = async ({ onSuccess, onFailed }: resultHandler) => {
        return await this.ordersCollection
                .orderBy('date', 'desc')
                .onSnapshot((querySnapshot) =>{  // Success
                    const resultData: groupOrderDate[] = []

                    querySnapshot.forEach(doc =>{
                        const docData = doc.data()
                        const tempDateIndex = resultData.findIndex(e => e.date == docData.date)
                        
                        if (tempDateIndex < 0){
                            resultData.push({
                                date: docData.date,
                                orders: [{
                                    id: docData.id,
                                    time: docData.time,
                                    items: docData.items,
                                    total: docData.total
                                }]
                            })
                        } else{
                            const temporderIdIndex = resultData[tempDateIndex]
                                                        .orders.findIndex(e => e == docData.id)

                            if (temporderIdIndex < 0) {
                                resultData[tempDateIndex].orders.push({
                                    id: docData.id,
                                    time: docData.time,
                                    items: docData.items,
                                    total: docData.total
                                })
                            } else {
                                resultData[tempDateIndex].orders[temporderIdIndex].items = docData.items
                            }
                        }
                    })
                    onSuccess(resultData)
                }, (error) => {               // Error
                    onFailed()
                    console.log(error)
                })
                
    }
}

export default FireStore