import { itemsType } from "../type";

const { SET_LOADING, GET_ITEMS, ADD_ITEMS } = itemsType

export const initialState = {
    loading: true,
    items: [],
    setLoading: (isLoad) => {}
}

const itemsReducer = (state = initialState, action) => {
    const { type, payload } = action

    switch (type){
        case SET_LOADING:
            return { ...state, loading: payload.loading }

        case GET_ITEMS:
            return { ...state, items: payload }

        case ADD_ITEMS:
            return { ...state, items: [...state.items, action.payload]   }
        
        case SET_LOADING:
            return { ...state, loading: payload.loading }
        
        default: 
            return state
    }
}

export default itemsReducer