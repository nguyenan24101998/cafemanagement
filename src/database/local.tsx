import { deleteDatabase, enablePromise, openDatabase } from "react-native-sqlite-storage";
import { Item } from "../models/item";


enablePromise(true);

const createTable = async (db) =>{
    await db.transaction(txn => {
        txn.executeSql(
            `CREATE TABLE IF NOT EXISTS items(
                id VARCHAR(255) PRIMARY KEY,
                label VARCHAR(255) NOT NULL,
                imgSrcOrUri VARCHAR(255) NOT NULL,
                unitPrice DECIMAL(19,4) NOT NULL
            );`, [],
            (sqlTxn, res) => {  },
            (error) => { console.log(error.message); }
        );
    })
}

const getDatabase = async () => {
    return openDatabase(
        { name: 'lafe.db'},
        () => { console.log("Database connected!") }, //on success, 
        (e) => { console.log(`Database access error. Code: ${e.code} - ${e.message} `) }
    )
}

class LocalDatabase {
    static db: any = null
    static itemTableName = 'items'

    static open = async () => {
        if (this.db == null){
            this.db = await getDatabase()
        }

        await createTable(this.db)
    }


    static deleteAllTables = async () => { //Use on devloping only
        if (this.db == null){
            this.db = await getDatabase()
        }
        this.db.executeSql('DROP TABLE items')
        this.db.executeSql('DROP TABLE orders')
        this.db.executeSql('DROP TABLE orderDetails')
    }

    static dropDatabase = async () => { //Use on devloping only
        deleteDatabase({name: "lafe.db"})
    }

    static getItems = async (): Promise<Item[]> => {
        if (this.db == null){
            throw Error("Database is null")
        }

        try {
            const items: Item[] = []
            const results = await this.db.executeSql(`SELECT * FROM ${this.itemTableName}`)

            results.forEach(result => {
                for (let index = 0; index  < result.rows.length; index++) {
                    items.push(result.rows.item(index));
                }
            });

            return items

        } catch (e) {
            console.log(e)
            throw Error("Failed")
        }
    }
    
    static addNewItem = async (item: Item) => {
        return await this.db.executeSql(`INSERT OR REPLACE INTO ${this.itemTableName} (id, label, imgSrcOrUri, unitPrice)` + 
            `VALUES ('${item.id}', '${item.label}', '${item.imgSrcOrUri}', ${item.unitPrice})`)
    }

    static addItems = async (items: Item[]) => {
        return await this.db.executeSql(
            `INSERT OR REPLACE INTO ${this.itemTableName} (id, label, imgSrcOrUri, unitPrice) VALUES` + 
            items.map(i => `('${i.id}', '${i.label}', '${i.imgSrcOrUri}', ${i.unitPrice})`).join(',')
        )
    }
}

export default LocalDatabase