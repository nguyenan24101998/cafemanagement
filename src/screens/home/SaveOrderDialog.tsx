import { memo } from "react";
import QueryDialog from "../../components/QueryDialog"
import { string } from "../../core/string";


const SaveOrderDialog = ({ isShow, onDismiss, onSubmit }) => (
    <QueryDialog
        isShow={ isShow }
        title="Hóa đơn sẽ được lưu vào dữ liệu"
        question="Lưu nhé ?"
        submitText={ string.label.buttonSave }
        onDismiss={ onDismiss }
        onSubmit={ onSubmit }
        onCancel={ onDismiss }/>
)

export default memo(SaveOrderDialog)