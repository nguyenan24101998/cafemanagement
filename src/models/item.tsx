import { idGenerator } from "../utils/UUID"

export class Item{
    id: string
    label:string
    imgSrcOrUri: string
    unitPrice: number

    constructor(label:string, imgSrcOrUri: string, unitPrice: number){
        this.id = idGenerator()
        this.label = label
        this.imgSrcOrUri = imgSrcOrUri
        this.unitPrice = unitPrice
    }
}