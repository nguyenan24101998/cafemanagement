import { memo } from "react";
import { theme } from "../core/theme";
import { ActivityIndicator } from 'react-native-paper'
import { StyleSheet } from "react-native";

type Props = {
    isShow: boolean
}

const Loading = ({isShow}: Props) => (
    <>
        {
            isShow ? (
                <ActivityIndicator
                    style={styles.loading}
                    size={35}
                    animating={true} 
                    color={theme.colors.primary} />
            ): null
        }
    </>
)

const styles = StyleSheet.create({
    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        opacity: 0.5,
        backgroundColor: theme.colors.dimLight,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default memo(Loading)