import { Text as ReactText } from "react-native-paper";
import { theme } from "../core/theme";
import { memo } from "react";

type Props = React.ComponentProps<typeof ReactText>

const Text = ({ style, children, ...props }: Props ) => (
    <ReactText 
        style={[
            {color: theme.colors.accent},
            style
        ]}
        {...props}>{children}</ReactText>
);

export default memo(Text)