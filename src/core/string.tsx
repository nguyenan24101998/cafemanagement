export const string = {
    label: {
        darkCafe: "Cafe đá",
        milkCafe: "Cafe sữa",
        vietnameseCafe: "Bạc xỉu",
        saltedCafe: "Cafe kem muối",
        creamyCafe: "Cafe kem",

        buttonSave: "Lưu",
        buttonCancel: "Hủy",
        textTotal: "Tổng cộng",
        ok: "Đồng ý"
    },

    status: {
        empty: "Chưa có gì hết á!!",
    }
}