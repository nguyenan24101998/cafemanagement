import { memo } from "react";
import { SafeAreaView as ReactSafeAreaView, StatusBar, StyleSheet } from "react-native";
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { theme } from "../core/theme";

type Props = React.ComponentProps<typeof ReactSafeAreaView>

const SafeAreaView = ({ children, style, ...props }: Props) => (
    <>
        <StatusBar translucent backgroundColor="transparent" barStyle='dark-content'/>
        <ReactSafeAreaView
            style={[
                styles.container,
                style
            ]}
            {...props}>        
            {children}
        </ReactSafeAreaView>
    </>
);

const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingTop: 25 + getStatusBarHeight(),
    },
});

export default memo(SafeAreaView)