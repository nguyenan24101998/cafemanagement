import { IconButton as PaperIconButton } from "react-native-paper";
import { theme } from "../core/theme";
import { memo } from "react";


type Props = React.ComponentProps<typeof PaperIconButton>

const IconButton = ({ style, ...props }: Props) => (
    <PaperIconButton
        style={[
            {backgroundColor: theme.colors.primary},
            style
        ]}
        iconColor={theme.colors.accent}
        {...props}/>
);

export default memo(IconButton)