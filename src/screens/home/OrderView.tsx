import { ReactElement, memo, useContext, useEffect, useState } from "react"
import { Alert, FlatList, StyleSheet, View } from "react-native"
import TonalButton from "../../components/TonalButton"
import { string } from "../../core/string"
import Text from "../../components/Text"
import IconButton from "../../components/IconButton"
import { theme } from "../../core/theme"
import Space from "../../components/Space"
import { vnCurrencyConvertor } from "../../utils/Convertor"
import VerticalLine from "../../components/VerticalLine"
import { OrderContext } from "../../context/orderdetail/orderdetail.context"
import ClearOrderDialog from "./ClearOrderDialog"
import SaveOrderDialog from "./SaveOrderDialog"
import FireStore from "../../database/firestore"
import { Order } from "../../models/order"
import { ItemContext } from "../../context/items/item.context"
import Loading from "../../components/Loading"

const OrderView = (): ReactElement => {
  const { orders, total, loading, isShowSaveDialog, isShowClearDialog, clearOrder
    , increaseAmount, decreaseAmount, saveOrderHandler, setShowSaveDialog
    , setShowClearDialog, undoHandler } = useContext(OrderContext)
  
  if (orders.length <= 0){
    return (<>{ null}</>)
  }

  return (
    <>
      <View style={styles.buttonWrapper}>
        <IconButton 
          style={styles.button} 
          icon='undo'
          iconColor={theme.colors.accent} 
          mode='contained-tonal'
          onPress={ undoHandler }
          />

        <TonalButton 
          style={styles.button} 
          onPress={ () => setShowClearDialog(true) }>
          {string.label.buttonCancel}
        </TonalButton>
      
        <Space width={3}/>

        <TonalButton 
          style={styles.button}
          onPress={ () => setShowSaveDialog(true) }>
          {string.label.buttonSave}
        </TonalButton>
        
        <Space width={3}/>
      </View>

      <View style={styles.listOrderWrapper}>
        <FlatList
          style={styles.listOrder}
          data={orders}
          scrollEnabled={true}
          keyExtractor={(_, index) => String(index)}
          renderItem={({item: detailRow, index}) =>
            <View style={styles.itemWrapper}>
              <Text style={styles.nameText}>{detailRow.item.label}</Text>
              <Text style={styles.unitPriceText}>{vnCurrencyConvertor(detailRow.item.unitPrice)}</Text>
              
              <IconButton
                  style={styles.indecreaseButton}
                  size={10}
                  icon='minus'
                  mode='contained-tonal'
                  onPress={() => decreaseAmount(index, detailRow.item.unitPrice)}/>
              
              <Text style={styles.xSign}>{`x${detailRow.amount}`}</Text>
              
              <IconButton
                  style={styles.indecreaseButton}
                  size={10}
                  icon='plus'
                  background={theme.colors.primary}
                  mode='contained-tonal'
                  onPress={() => increaseAmount(index, detailRow.item.unitPrice)}/>

              <Text style={styles.priceText}>
                  {detailRow.item.unitPrice === 0 ? '' : 
                  vnCurrencyConvertor(detailRow.item.unitPrice*detailRow.amount)}
              </Text>
            </View>
          }/>

        <VerticalLine width='100%'/>
        
        <View style={styles.totalText}>
          <Text>
            {`${string.label.textTotal}: ${vnCurrencyConvertor(total)}`}
          </Text>
        </View>

        <SaveOrderDialog
          isShow={ isShowSaveDialog }
          onDismiss={ () => setShowSaveDialog(false) }
          onSubmit={ saveOrderHandler }/>
        
        <ClearOrderDialog
          isShow={ isShowClearDialog }
          onDismiss={ () => setShowClearDialog(false) } 
          onSubmit={ clearOrder }/>

      </View>
      <Loading isShow={loading}/>
    </>
  )
}

const styles = StyleSheet.create({
  buttonWrapper: { 
      paddingBottom: 2, 
      flexDirection: 'row', 
      justifyContent: 'flex-end' 
  },

  button: { 
      alignSelf:'baseline'
  },

  listOrderWrapper: {
      height:'30%', 
      backgroundColor:'#fff'
  },

  listOrder: {
    padding: 10,
    flex: 0.7,
  },

  indecreaseButton: {
      width:'6%', 
      aspectRatio:1
  },

  itemWrapper: {
      flexDirection: 'row', 
      alignItems: 'center'
  },

  nameText: {
      width: '30%'
  },

  unitPriceText: {
      width: '25%'
  },

  xSign: {
      width: '5%', 
      textAlign:'center'
  },

  priceText: {
      width: '25%', 
      textAlign:'left'
  },

  totalText: {
      flex: 0.15, 
      flexDirection: 'row', 
      padding: 10, 
      paddingBottom: 15, 
      justifyContent: 'flex-end'
  }
});

export default memo(OrderView)