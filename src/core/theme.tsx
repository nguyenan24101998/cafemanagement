import { DefaultTheme } from 'react-native-paper';

export const theme = {
    ...DefaultTheme,
    colors: {
        ...DefaultTheme.colors,
        primary: '#c9fec1',
        accent: '#665444',
        statusBar: '#f2f3f3',
        button: '#ecebeb',
        text: '#1fb93e',
        error: '#f13a59',
        success: '#e8ebea',
        dimLight: 'rgba(0,0,0,0.6)',
    }
};