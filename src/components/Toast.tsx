import React, { memo } from 'react';
import { View, Text, StyleSheet } from 'react-native'
import { Snackbar } from 'react-native-paper';
import { theme } from '../core/theme';

type Props = {
    message: string | boolean;
    onDismiss(): void;
    onUndoPress(): void;
    type?: string;
};

const Toast = ({ type="error", message, onDismiss, onUndoPress }: Props) => (
    <View style={styles.container}>
        <Snackbar
         visible={!!message}
         duration={3000}
         onDismiss={onDismiss}
         action={{
            label: 'Undo',
            onPress: onUndoPress,
            textColor: theme.colors.text
         }}
         style={{ 
            backgroundColor:
                type === "error" ? theme.colors.error : theme.colors.success
          }} >
            <Text style={styles.content}>{message}</Text>
        </Snackbar>
    </View>
);

const styles = StyleSheet.create({
    container: {
      position: "absolute",
      bottom: 50,
      width: "100%",
      alignSelf: "center"
    },
    content: {
      fontWeight: "500"
    }
  });

export default memo(Toast)