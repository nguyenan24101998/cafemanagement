import { ReactElement, memo, useContext } from "react";
import { FlatList, StyleSheet } from "react-native";
import { Surface } from 'react-native-paper';
import ImageButton from "../../components/ImageButton";
import { theme } from "../../core/theme";
import VerticalLine from "../../components/VerticalLine";
import { ItemContext } from "../../context/items/item.context";
import { defaultItemImageCollection } from "../../utils/DefaultImagePath";
import { OrderContext } from "../../context/orderdetail/orderdetail.context";

type Props = {
  numColumns: number
}

const ItemList = ({numColumns}: Props): ReactElement => {
  const { items } = useContext(ItemContext) as any
  const { addOrUpdateOrder } = useContext(OrderContext)
  
  return (
      <FlatList
          style={{paddingLeft: 35, paddingRight: 35}}
          columnWrapperStyle={{justifyContent:'space-between'}}
          data={items}
          numColumns={numColumns}
          key={numColumns}
          keyExtractor={ (_, index) => String(index) }
          contentContainerStyle={{}}
          renderItem={({item}) => 
          <Surface style={styles.buttonStyle}>
              <ImageButton
                  imageStyle={styles.buttonImageIconStyle}
                  textStyle={styles.buttonTextStyle}
                  source={defaultItemImageCollection[item.imgSrcOrUri]}
                  onPress={ () => addOrUpdateOrder(item) }
                  label={item.label}>
              
              <VerticalLine width={60} styles={styles.buttonIconSeparatorStyle}/>
              
              </ImageButton>
          </Surface>
      }/>
  )
}

const styles = StyleSheet.create({
    buttonStyle: {
     justifyContent:'center',
     width: 145,
     height:125,
     backgroundColor: theme.colors.button,
     borderWidth: 0.5,
     borderColor: theme.colors.button,
     borderRadius: 10,
     margin: 10
   },

   buttonImageIconStyle: {
    margin: 5,
    height: 55,
    width: 55,
    resizeMode: 'stretch',
    alignSelf:'center'
  },

  buttonTextStyle: {
    marginTop: 5,
    alignSelf:'center'
  },

  buttonIconSeparatorStyle: {
    alignSelf:'center'
  },
})

export default memo(ItemList)