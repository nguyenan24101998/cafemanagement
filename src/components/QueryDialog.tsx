import { Dialog, Portal } from "react-native-paper";
import TextButton from "./TextButton";
import Text from "./Text";
import { string } from "../core/string";
import { memo } from "react";
import { StyleSheet } from "react-native";

const QueryDialog = ({ isShow, title, question, onDismiss, submitText, onSubmit, onCancel}) => (
    <Portal>
        <Dialog visible={ isShow }
            style={ styles.dialogFrame }
            onDismiss={ onDismiss }>
            <Dialog.Title>{ title }</Dialog.Title>
            <Dialog.Content>
                <Text variant="bodyMedium">{ question }</Text>
            </Dialog.Content>
            <Dialog.Actions>
                <TextButton uppercase={true} onPress={ onCancel }>{ string.label.buttonCancel }</TextButton>
                <TextButton uppercase={true} onPress={ onSubmit }>{ submitText }</TextButton>
            </Dialog.Actions>
        </Dialog>
    </Portal>
)

const styles = StyleSheet.create({
    dialogFrame:{
        borderRadius: 7
    },

    button: {
        width: '20%'
    }
})

export default memo(QueryDialog)