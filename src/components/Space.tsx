import { memo } from "react"
import { DimensionValue, View } from "react-native"

type Props = {
    width: DimensionValue , 
    height: DimensionValue
}

const Space = ({width, height}: Props) => (
    <View style={{ width: width, height: height }}/>
)

Space.defaultProps = {
    with: 0,
    height: 0
}

export default memo(Space)