import { OrderDetail, OrderDetailWithIdOnly } from "../type"
import { getTime, getToday } from "../utils/DateTime"
import { idGenerator } from "../utils/UUID"

/** 
 * schema: 
 * "00/00/00":[{
 *     "detail":{
 *          orderDetailId:{
 *              item: []
 *              time: "00:00"
 *              total: 0000
 *          }
*      }
 * }],
 * "01/01/01":[{
 *     "detail":{
 *          orderDetailId:{
 *              item: []
 *              time: "01:01"
 *              total: 0001
 *          }
*      }
 * }],
 * .....
**/


export class Order  {
    id: string = idGenerator()
    date: string = getToday()
    time: string = getTime()
    items: OrderDetailWithIdOnly[] = []
    total: number = 0
    isPaid: boolean = false
}