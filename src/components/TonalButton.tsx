import React, { memo } from "react";
import { Button } from "react-native-paper";
import { theme } from "../core/theme";

type Props = React.ComponentProps<typeof Button>;

const TonalButton = ({ children, ...props }: Props) => (
  <Button
    buttonColor={theme.colors.primary}
    labelStyle={{color: theme.colors.accent}}
    mode="contained-tonal"
    {...props}>
    {children}
  </Button>
);

export default memo(TonalButton);