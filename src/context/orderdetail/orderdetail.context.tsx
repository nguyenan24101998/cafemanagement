import { ReactElement, createContext, useReducer, useContext } from "react"
import { orderType } from "../type"
import ordersReducer, { initialState } from "./orderdetail.reducer"
import { Order } from "../../models/order"
import FireStore from "../../database/firestore"
import { Alert } from "react-native"
import { ItemContext } from "../items/item.context"
import { cloneDeep } from 'lodash'

export const OrderContext = createContext(initialState)

const { UPDATE_ORDER, UPDATE_DETAIL, SET_CACHE, SET_CACHE_DETAIL
    , UPDATE_TOTAL, SET_SAVE_DIALOG, SET_CLEAR_DIALOG, SET_CACHE_TOTAL } = orderType

const OrderProvider = ({children}): ReactElement => {
    const [state, dispatch] = useReducer(ordersReducer, initialState)
    const { setLoading } = useContext(ItemContext)

    const dispatchOrder = () => {
        dispatch({
            type: UPDATE_ORDER,
            payload: {
                orders: [...state.orders]
            }
        })

        dispatch({
            type: UPDATE_DETAIL,
            payload: {
                orderDetail: [...state.orderDetail]
            }
        })
    }

    const addOrUpdateOrder = (newItem) => {
        dispatchCache()
    
        const position = state.orders.findIndex(e => e.item.id === newItem.id)

        if (position >= 0) {  // item has been on order
            increaseAmount(position, newItem.unitPrice)
            return
        }
        // else - add new item to order
        state.orders.push({ item: newItem, amount: 1 })
        state.orderDetail.push({ itemId: newItem.id, amount: 1 }) // for saving to firebase
        
        dispatchOrder()
        updateTotal(state.total + newItem.unitPrice)
    }

    const dispatchCache = () => {
        dispatch({
            type: SET_CACHE,
            payload: {
                cacheOrder: [...state.cacheOrder, cloneDeep(state.orders)]
            }
        })

        dispatch({
            type: SET_CACHE_DETAIL,
            payload: {
                cacheOrderDetail: [...state.cacheOrderDetail, cloneDeep(state.orders)]
            }
        })

        dispatch({
            type: SET_CACHE_TOTAL,
            payload: {
                cacheTotal: [...state.cacheTotal, state.total]
            }
        })
    }

    const undoHandler = () => {
        const orderLastIndex = state.cacheOrder.length - 1
        
        state.orders = state.cacheOrder[orderLastIndex]
        state.orderDetail = state.cacheOrderDetail[orderLastIndex]

        dispatchOrder()
        updateTotal(state.cacheTotal[state.cacheTotal.length - 1])

        state.cacheOrder.splice(-1)
        state.cacheOrderDetail.splice(-1)
        state.cacheTotal.splice(-1)
    }

    const increaseAmount = (index, unitPrice) => {
        state.orders[index].amount++
        state.orderDetail[index].amount++
        
        dispatchOrder()
        updateTotal(state.total + unitPrice)
    }

    const decreaseAmount = (index, unitPrice) => {
        if (state.orders[index].amount == 1){
            state.orders.splice(index, 1)
            state.orderDetail.splice(index, 1)
        }else{
            state.orders[index].amount--
            state.orderDetail[index].amount--
        }
        
        dispatchOrder()
        updateTotal(state.total - unitPrice)
    }

    const updateTotal = (total=0) => {
        dispatch({
            type: UPDATE_TOTAL,
            payload: { 
                total: total 
            }
        });
    }

    const clearOrder = () => {
        state.orders = []
        state.orderDetail = []
        
        dispatchOrder()
        updateTotal(0)

        //clear cache
        dispatch({
            type: SET_CACHE,
            payload: {
                cacheOrder: []
            }
        })

        dispatch({
            type: SET_CACHE_DETAIL,
            payload: {
                cacheOrderDetail: []
            }
        })

        dispatch({
            type: SET_CACHE_TOTAL,
            payload: {
                cacheTotal: []
            }
        })
        
        setShowClearDialog(false)
    }

    const saveOrderHandler = () => {
        setShowSaveDialog(false)
        setLoading(true)
        
        const order = new Order()
        order.items = state.orderDetail
        order.total = state.total
      
        FireStore.addOrUpdateOrder(order, {
          onSuccess: (_) => {
              Alert.alert("Lưu thành công!")
              setLoading(false)
              clearOrder()
          },
          onFailed: () => {
              Alert.alert("Lưu không thành công!") 
              setLoading(false)
          }
        })
      }

    const setShowSaveDialog = (isShow) => {
        dispatch({
            type: SET_SAVE_DIALOG,
            payload: {
                isShowSaveDialog: isShow
            }
        })
    }

    const setShowClearDialog = (isShow) => {
        dispatch({
            type: SET_CLEAR_DIALOG,
            payload: {
                isShowClearDialog: isShow
            }
        })
    }

    const value = {
        orders: state.orders,
        orderDetail: state.orderDetail,
        cacheOrder: state.cacheOrder,
        cacheOrderDetail: state.cacheOrderDetail,
        cacheTotal: state.cacheTotal,
        total: state.total,
        loading: state.loading,
        isShowSaveDialog: state.isShowSaveDialog,
        isShowClearDialog: state.isShowClearDialog,
        addOrUpdateOrder,
        clearOrder,
        increaseAmount,
        decreaseAmount,
        undoHandler,
        saveOrderHandler,
        setShowSaveDialog,
        setShowClearDialog,
    }

    return (
        <OrderContext.Provider value={value}>{children}</OrderContext.Provider>
    )
}

export default OrderProvider