import { ReactNode, memo } from "react";
import { View, StyleSheet } from "react-native";
import SafeAreaView from '../components/SafeAreaView';
import Text from '../components/Text';

/* 
    A note wrapper. Use when data is empty or loading faild (to note user)
    Eg: User's first time install app => Data is empty => Show on screen "Data is empty"
     * conditionToShowText={data.length <= 0} or {data == null}
     * text="Data is empty"
*/

type Props = {
    conditionToShowText: boolean;
    text: string;
    children: ReactNode
};

const StatusTextWrapper = ({conditionToShowText, text, children}: Props) => (
    <>
        { conditionToShowText ?
            (
                <View style={ styles.container }>
                    <Text style={{ fontSize: 16 }}>{text}</Text>
                </View>
            ) :
            (
                <SafeAreaView>
                    { children }
                </SafeAreaView>
            )
        }
    </>
);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center', 
        alignItems:'center',
    }
})

export default memo(StatusTextWrapper)
