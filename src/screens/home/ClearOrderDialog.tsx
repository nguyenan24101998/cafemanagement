import { memo } from "react"
import QueryDialog from "../../components/QueryDialog"
import { string } from "../../core/string"

const ClearOrderDialog = ({ isShow, onDismiss, onSubmit }) => (
    <QueryDialog
        isShow={ isShow }
        title="Hóa đơn sẽ bị xóa"
        question="Xóa nhé ?"
        submitText={ string.label.ok }
        onDismiss={ onDismiss }
        onCancel={ onDismiss }
        onSubmit={ onSubmit }/>
)

export default memo(ClearOrderDialog)