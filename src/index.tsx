// import { createAppContainer } from "react-navigation"
// import { createStackNavigator } from "react-navigation-stack";
import React from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import {
    HomeScreen, 
    SoldHistoryScreen,
} from "./screens"
import ItemProvider from './context/items/item.context';


const Router = createStackNavigator();

const appContainter = () => (
    <ItemProvider>
        <NavigationContainer>
            <Router.Navigator initialRouteName="HomeScreen" screenOptions={{headerShown: false}}>
                <Router.Screen name='HomeScreen' component={ HomeScreen }/>
                <Router.Screen name='SoldHistoryScreen' component={ SoldHistoryScreen }/>
            </Router.Navigator>
        </NavigationContainer>
    </ItemProvider>
)

export default appContainter